 # Net analyzer v0.1 
 # Author xOn
 # Script for automated analysis of network dumps (pcap files)
 #

 #!/bin/bash
 ARGS=1
 E_BADARGS=85

 if [ $# -ne "$ARGS" ]
 then
 echo "Usage: `basename $0` <pcap file>"
 exit $E_BADARGS
 fi




{ NUMBER_OF_PACKETS="$(tshark -r $1  | awk {'print $1'} | tail -n 1)";} 2>/dev/null 

{ NUMBER_OF_ICMP="$(tshark -r $1 icmp | wc | awk {'print $1'})";} 2>/dev/null

{ NUMBER_OF_TCP="$(tshark -r $1 tcp | wc | awk {'print $1'})";} 2>/dev/null

{ NUMBER_OF_UDP="$(tshark -r $1 udp | wc | awk {'print $1'})";} 2>/dev/null

{ NUMBER_OF_HTTP="$(tshark -r $1 http | wc | awk {'print $1'})";} 2>/dev/null

{ NUMBER_OF_TCPFLOWS="$(tshark -r $1 -T fields -e tcp.stream | sort -g | tail -1)";} 2>/dev/null

{ NUMBER_OF_UDPFLOWS="$(tshark -r $1 -T fields -e udp.stream | sort -g | tail -1)";} 2>/dev/null

{ NUMBER_OF_DNS_TRAFFIC="$(tshark -r $1 dns | wc -l | tail -n 1)";} 2>/dev/null


NUMBER_OF_FIXED_TCPFLOWS=$((NUMBER_OF_TCPFLOWS + 1))
NUMBER_OF_FIXED_UDPFLOWS=$((NUMBER_OF_UDPFLOWS + 1))


{ DNS_TOTAL_RESOLVE="$(tshark -r $1 -T fields -e ip.src -e dns.qry.name -Y "dns.flags.response eq 0" | wc  | awk {'print $1'})" ;} 2>/dev/null

HTTP_PERC="$(python -c "print float( ('"${NUMBER_OF_HTTP//\'/\\\'}"'))*100 / float(('"${NUMBER_OF_TCP//\'/\\\'}"'))   ")"

UDP_PERC="$(python -c "print float( ('"${NUMBER_OF_FIXED_UDPFLOWS//\'/\\\'}"'))*100 / float(('"${NUMBER_OF_TCP//\'/\\\'}"'))   ")"

DNS_PERC="$(python -c "print float( ('"${NUMBER_OF_DNS_TRAFFIC//\'/\\\'}"'))*100 / float(('"${NUMBER_OF_TCP//\'/\\\'}"'))   ")"


echo "$(tput setaf 6) Select Option: "
echo "$(tput setaf 6)1) Baseline Analysis (Protocols used)"
echo "2) Remote Hosts Enumeration"
echo "3) Print all TCP flows"
echo "4) Print Protocol Hierarch"
echo "5) Extracting all files using foremost"
echo "6) Investigate for IOC "
echo "7) Replay session "


while true; do
    read -p "Choose Option: " n

    case $n in
    1)  echo "Baseline Analysis"


        echo "=============================================================="
        echo "Pcap File details: "

	capinfos $1

        echo "=============================================================="

	echo "Number of Packets: ${NUMBER_OF_PACKETS}"
	echo "Number of ICMP packets: ${NUMBER_OF_ICMP}"
	echo "Number of TCP packets: ${NUMBER_OF_TCP}"
	echo "Number of UDP packets: ${NUMBER_OF_UDP}"
	echo "Number of HTTP packets: ${NUMBER_OF_HTTP} Percent of HTTP / TCP: ${HTTP_PERC}"
	echo "Number of DNS traffic (Total): ${NUMBER_OF_DNS_TRAFFIC} Percent of DNS / TCP: ${DNS_PERC}"

	echo "Number of TCP Flows: ${NUMBER_OF_FIXED_TCPFLOWS}"
	echo "Number of UDP Flows: ${NUMBER_OF_FIXED_UDPFLOWS}"
	
	#echo "Specify the source IP address to be excluded"
	#echo "Press 1 to return or anything else to exit: ${CHOICE}"
	#read i 

	echo ""

        #echo "Press 1 to return or anything else to exit: ${CHOICE}"
        ;;

    2) echo "Starting Hosts Enumeration"

               echo "EOF";; 

    3) echo "Printing all TCP flows..."

           printf "\n"
	   tshark -r $1 -qz conv,tcp 2>1& /dev/null
		
           echo "End of TCP flows";;

    4)  echo "Printing Protocol Hierarchy"

	echo "Packets will usually contain multiple protocols, so more than one protocol
will be counted for each packet. Example: IP has 99,00%
and TCP 85,00% (which is together much more than 100%)."

	tshark -qz io,phs -r $1 2>/dev/null

	;;

    5)  echo "Extracting all files from network capture"
	foremost -v -i $1
	;;
    6)	echo "Searching for IOC"

	echo "Printing all DNS query names"
	tshark -r $1 -T fields -e ip.src -e dns.qry.name -Y "dns.flags.response eq 0" 2>/dev/null

	echo "DNS is oftenly used from malware and for exfiltration. It is common to see botnets using DNS for exfiltrating traffic"
	echo "Total number of DNS name queries: ${DNS_TOTAL_RESOLVE}"

	echo "====================================================="
	echo "Searching in bandwidth spikes or anomalies"

	# Interval by default is 5 seconds - this should be changed depending on situation
	# Bandwidth spikes can be also meassured against specific traffic... For example
	#  tshark -a duration:60  -q -z io,stats,5  -R 'syslog.msg contains FE-3KD3R11000037' port 514
	# the above reads the information that a fortinet firewall writes on the syslog server
	# other useful filters for syslog --> tshark -a duration:60  -R  'syslog.facility==19' -q -z io,stat,5 port 514 
	# and also filtering informational messages (or others...) with
	# tshark -n -a duration:60  -R  'syslog.level==6' -qz io,stat,1  port 514 
	# Final  example; let's get creative and measure traffic for a particular host and at  1sec interval for informational  and ssh2 request for src network 10.8.23
	#tshark -n -R '(syslog.level==5) and (syslog.msg contains SSH)' -q -z io,stat,1 -a duration:60  port 514

	tshark -r $1 -q -z io,stat,5 2>/dev/null
	echo "================================================================"
	echo "|Printing for DNS NX (Non-Existent) and other SERVFAIL messages |"

	tshark -r $1 -T fields -e dns.qry.name -e dns.flags.rcode -Y "dns.qry.name contains drush and dns.flags.response eq 1 and dns.flags.rcode != 0" 2>/dev/null
	echo "================================================================"
	tshark -r $1 -Y "dns.flags.response eq 1 and dns.flags.rcode!=0" -T fields -e dns.qry.name -e dns.flags.rcode 2>/dev/null
        echo "";;

    *)  echo invalid option
	break
    ;;

   esac
done
